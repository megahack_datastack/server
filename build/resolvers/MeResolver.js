"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const owl_1 = require("@zuu/owl");
const User_1 = require("../model/entities/User");
const Document_1 = require("../model/entities/Document");
const Uploaded_1 = require("../model/entities/Uploaded");
const FaceApi_1 = require("../packages/faces/FaceApi");
const Authorization_1 = require("../model/entities/Authorization");
const Signature_1 = require("../model/entities/Signature");
const Contract_1 = require("../model/entities/Contract");
const RequiredResourceNotProvidedError_1 = require("../errors/RequiredResourceNotProvidedError");
const SignedContract_1 = require("../model/entities/SignedContract");
const speakeasy = require("speakeasy");
const qr = require("qrcode");
const fs = require("fs");
const path = require("path");
let MeResolver = class MeResolver {
    me(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return user;
        });
    }
    ;
    document(args, user) {
        return __awaiter(this, void 0, void 0, function* () {
            let doc = new Document_1.Document(args);
            yield doc.save();
            (yield user.documents).push(doc);
            if (doc.uploaded) {
                let uploaded = yield Uploaded_1.Uploaded.findOne(doc.uploaded);
                if (!!uploaded) {
                    let faces = new FaceApi_1.FaceApi();
                    let faceResult = yield faces.uploadFaces(uploaded.path);
                    if (faceResult.length != 1) {
                        throw new Error('Invalid photo!');
                    }
                    let face = faceResult[0];
                    doc.faceId = face.faceId;
                    user.faceId = face.faceId;
                }
            }
            yield user.save();
            yield doc.save();
            return doc;
        });
    }
    ;
    authorize(uploadedId, user) {
        return __awaiter(this, void 0, void 0, function* () {
            let auth;
            if (!user.faceId) {
                auth = new Authorization_1.Authorization(user.id, false);
                yield auth.save();
                return auth;
            }
            let uploaded = yield Uploaded_1.Uploaded.findOne(uploadedId);
            if (!uploaded) {
                auth = new Authorization_1.Authorization(user.id, false);
                yield auth.save();
                return auth;
            }
            try {
                let faces = new FaceApi_1.FaceApi();
                let faceResult = yield faces.uploadFaces(uploaded.path);
                if (faceResult.length != 1) {
                    auth = new Authorization_1.Authorization(user.id, false);
                    yield auth.save();
                    return auth;
                }
                let face = faceResult[0];
                let match = yield faces.compareFaces(user.faceId, face.faceId);
                console.log(face, match);
                if (!match.isIdentical) {
                    auth = new Authorization_1.Authorization(user.id, false);
                    yield auth.save();
                    return auth;
                }
            }
            catch (ex) {
                auth = new Authorization_1.Authorization(user.id, false);
                yield auth.save();
                return auth;
            }
            auth = new Authorization_1.Authorization(user.id, true);
            yield auth.save();
            return auth;
        });
    }
    ;
    authorizeUsing2FA(tfa, user) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!user.has2FAActivated) {
                let auth = new Authorization_1.Authorization(user.id, false);
                yield auth.save();
                return auth;
            }
            let verified = speakeasy.totp.verify({
                secret: user.twoFactorToken,
                encoding: 'base32',
                token: tfa
            }) || false;
            let auth = new Authorization_1.Authorization(user.id, verified);
            yield auth.save();
            return auth;
        });
    }
    ;
    activate2FA(authorizationId, user) {
        return __awaiter(this, void 0, void 0, function* () {
            let authorization = yield Authorization_1.Authorization.findOne(authorizationId);
            if (!authorization)
                return false;
            if (!authorization.authorized)
                return false;
            if (authorization.user != user.id)
                return false;
            let token = speakeasy.generateSecret({ length: 20 });
            user.twoFactorToken = token.base32;
            yield user.save();
            let code = yield qr.toDataURL(token.otpauth_url);
            fs.writeFileSync(path.join(__dirname, "../../qr.html"), `
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>QR</title>
            </head>
            <body>
                <img src="${code}">
            </body>
            </html>
        `);
            return true;
        });
    }
    ;
    sign(authorizationId, uploadedId, contractId, user) {
        return __awaiter(this, void 0, void 0, function* () {
            let uploaded = yield Uploaded_1.Uploaded.findOne(uploadedId);
            if (!uploaded)
                return null;
            let authorization = yield Authorization_1.Authorization.findOne(authorizationId);
            if (!authorization)
                return null;
            if (!authorization.authorized)
                return null;
            if (authorization.user != user.id)
                return null;
            let contract = yield Contract_1.Contract.findOne(contractId);
            if (!contract)
                return null;
            let signature = new Signature_1.Signature();
            signature.uploaded = uploadedId;
            yield user.save();
            (yield contract.signatures).push(signature);
            yield signature.save();
            yield contract.save();
            yield user.save();
            (yield user.signatures).push(signature);
            (yield user.contracts).push(contract);
            yield signature.save();
            yield contract.save();
            yield user.save();
            return signature;
        });
    }
    signedContract(contract, user) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!contract)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError('contract');
            let sc = new SignedContract_1.SignedContract();
            sc.contract = contract;
            let signature = yield Signature_1.Signature.findOne({ where: { user, contract } });
            if (!!signature) {
                sc.signature = signature;
                sc.qr = yield qr.toDataURL(signature.id);
            }
            return sc;
        });
    }
};
__decorate([
    owl_1.Query(returns => User_1.User),
    __param(0, owl_1.Ctx('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_1.User]),
    __metadata("design:returntype", Promise)
], MeResolver.prototype, "me", null);
__decorate([
    owl_1.Mutation(returns => Document_1.Document),
    __param(0, owl_1.Args(type => Document_1.DocumentArgs)),
    __param(1, owl_1.Ctx('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Document_1.DocumentArgs,
        User_1.User]),
    __metadata("design:returntype", Promise)
], MeResolver.prototype, "document", null);
__decorate([
    owl_1.Mutation(type => Authorization_1.Authorization),
    __param(0, owl_1.Arg('uploaded', { nullable: false })),
    __param(1, owl_1.Ctx('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, User_1.User]),
    __metadata("design:returntype", Promise)
], MeResolver.prototype, "authorize", null);
__decorate([
    owl_1.Mutation(type => Authorization_1.Authorization),
    __param(0, owl_1.Arg('tfa', { nullable: false })),
    __param(1, owl_1.Ctx('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, User_1.User]),
    __metadata("design:returntype", Promise)
], MeResolver.prototype, "authorizeUsing2FA", null);
__decorate([
    owl_1.Mutation(type => Boolean),
    __param(0, owl_1.Arg('authorization', { nullable: false })),
    __param(1, owl_1.Ctx('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, User_1.User]),
    __metadata("design:returntype", Promise)
], MeResolver.prototype, "activate2FA", null);
__decorate([
    owl_1.Mutation(type => Signature_1.Signature),
    __param(0, owl_1.Arg('authorization', { nullable: false })),
    __param(1, owl_1.Arg('uploaded', { nullable: false })),
    __param(2, owl_1.Arg('contract', { nullable: false })),
    __param(3, owl_1.Ctx('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, User_1.User]),
    __metadata("design:returntype", Promise)
], MeResolver.prototype, "sign", null);
__decorate([
    owl_1.Query(type => SignedContract_1.SignedContract, { nullable: true }),
    __param(0, owl_1.Ctx('contract')),
    __param(1, owl_1.Ctx('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Contract_1.Contract,
        User_1.User]),
    __metadata("design:returntype", Promise)
], MeResolver.prototype, "signedContract", null);
MeResolver = __decorate([
    owl_1.Resolver()
], MeResolver);
exports.MeResolver = MeResolver;
//# sourceMappingURL=MeResolver.js.map