"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bootstrap_1 = require("@zuu/bootstrap");
const mink_1 = require("@zuu/mink");
const vet_1 = require("@zuu/vet");
const AuthController_1 = require("./controllers/AuthController");
const UploadController_1 = require("./controllers/UploadController");
const HomeController_1 = require("./controllers/HomeController");
const ResponseFormatter_1 = require("./interceptors/ResponseFormatter");
const ListeningEventListener_1 = require("./listeners/ListeningEventListener");
const SubscriptionServerListeningEventListener_1 = require("./listeners/SubscriptionServerListeningEventListener");
const ResponseTime_1 = require("./middlewares/ResponseTime");
const User_1 = require("./model/entities/User");
const ExpressHandlebarsRenderer_1 = require("./modules/ExpressHandlebarsRenderer");
const AuthContextChecker_1 = require("./packages/context/AuthContextChecker");
const HeadersContextFiller_1 = require("./packages/context/HeadersContextFiller");
const Timer_1 = require("./packages/timer/Timer");
const MeResolver_1 = require("./resolvers/MeResolver");
const Provider_1 = require("./model/entities/Provider");
const Contract_1 = require("./model/entities/Contract");
const ProviderResolver_1 = require("./resolvers/ProviderResolver");
vet_1.Debugger.deafults();
let tag = vet_1.Debugger.tag('app-index');
let options = {
    server: {
        port: parseInt(process.env['PORT']) || 4100,
        modules: [
            new ExpressHandlebarsRenderer_1.ExpressHandlebarsRenderer({
                defaultLayout: 'main'
            })
        ]
    },
    currentUserChecker: (action) => __awaiter(this, void 0, void 0, function* () {
        let token = action.request.headers['x-access-token'];
        return yield AuthContextChecker_1.AuthContextChecker.check(token);
    }),
    graph: {
        contextFiller: (user, headers) => __awaiter(this, void 0, void 0, function* () {
            return yield HeadersContextFiller_1.HeadersContextFiller.fill(user, headers);
        }),
        subscriptionCurrentUserChecker: (connectionParams, webSocket, context) => __awaiter(this, void 0, void 0, function* () {
            let token = connectionParams['x-access-token'];
            let user = yield AuthContextChecker_1.AuthContextChecker.check(token);
            if (!user)
                throw new mink_1.UnauthorizedError();
            return Object.assign({ user }, (yield HeadersContextFiller_1.HeadersContextFiller.fill(user, options)));
        })
    },
    listeners: [
        new ListeningEventListener_1.ListeningEventListener,
        new SubscriptionServerListeningEventListener_1.SubscriptionServerListeningEventListener
    ],
    middlewares: [
        ResponseTime_1.ResponseTime
    ],
    interceptors: [
        ResponseFormatter_1.ResponseFormatter
    ],
    controllers: [
        HomeController_1.HomeController,
        AuthController_1.AuthController,
        UploadController_1.UploadController,
        bootstrap_1.AuthedGraphQLController
    ],
    resolvers: [
        MeResolver_1.MeResolver,
        ProviderResolver_1.ProviderResolver
    ],
    cors: true
};
let lorem = `Lorem ipsum dolor sit amet, vivendo minimum facilisi nam ut, quis nihil facete no eum. Pri no minimum nominavi delectus, eam odio modus definitiones ut. Dolore comprehensam sit ea, vis pertinacia expetendis at. Est vocibus mentitum et.
Audire delenit mandamus pro in, ex sea veniam omittantur. Offendit consequat comprehensam nec at. Id elitr consequuntur nam. Ad per modus scripta conceptam, solet dolorum sea ad, has ipsum simul iisque ei.
Ei diceret denique ius, ne latine electram ius. Sed solet aliquando id, ea elitr dicant expetenda vel. Blandit platonem persequeris ea sea, ea eam rebum mazim. Admodum scribentur ei vix, vim te etiam dicunt. Vis debet ludus id. An duo nostrum perpetua.
No has dicant scripta. Patrioque referrentur ei vel, ius animal qualisque deterruisset in. Duo nibh iisque at, ius at omnium aperiam virtute. Ei vix alii dolorem minimum, sumo democritum usu ne, eam libris epicuri ex.
Tollit soluta dolores sit et, sea viris epicuri conclusionemque cu. Populo primis ut mea, duis delectus platonem quo cu. Vim ex falli volumus ullamcorper, altera melius persius sed id. Mel at debet labores, dicta decore menandri ei duo.`;
function testData() {
    return __awaiter(this, void 0, void 0, function* () {
        let user = yield User_1.User.findOne({ where: { email: "datastack@inno.ro" } });
        let provider = new Provider_1.Provider(user);
        provider.name = "InnoEnergy";
        yield provider.save();
        // for(let i = 1; i <= 3; i++) {
        let contract = new Contract_1.Contract();
        contract.name = "Contract INNO";
        contract.description = `${lorem}`;
        contract.preview = "fbea0baa-1d65-4634-8c92-4b3140189df8";
        contract.uploaded = "3aa354be-f640-47f8-a035-87842ecc1680";
        yield contract.save();
        (yield provider.contracts).push(contract);
        yield provider.save();
        yield contract.save();
        // }    
    });
}
let timer = new Timer_1.Timer().reset();
vet_1.Runtime.scoped(null, (_) => __awaiter(this, void 0, void 0, function* () {
    vet_1.Debugger.log(tag `Initialization began!`);
    let { app } = yield bootstrap_1.Bootstrap.scope(options).run();
    return (typeof app != 'undefined' && app != null);
}))
    .then((result) => __awaiter(this, void 0, void 0, function* () {
    vet_1.Debugger.log(tag `Initialization succeeded! Took ${timer.stop().diff()}ms!`);
    // await testData();
}))
    .catch(vet_1.Debugger.error);
//# sourceMappingURL=index.js.map