"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bootstrap_1 = require("@zuu/bootstrap");
const hdbs = require("express-handlebars");
const vet_1 = require("@zuu/vet");
const bodyParser = require("body-parser");
let tag = vet_1.Debugger.tag('express-handlebars-renderer');
class ExpressHandlebarsRenderer extends bootstrap_1.AbstractModule {
    constructor(options = {}) {
        super([bootstrap_1.LoadType.BEFORE]);
        this.options = options;
    }
    handleBefore(app) {
        app.use(bodyParser.json({ limit: '50mb' }));
        let exhdbs = hdbs.create(this.options);
        app.engine('handlebars', exhdbs.engine);
        app.set('view engine', 'handlebars');
        vet_1.Debugger.log(tag `Module loaded!`);
        return app;
    }
}
exports.ExpressHandlebarsRenderer = ExpressHandlebarsRenderer;
//# sourceMappingURL=ExpressHandlebarsRenderer.js.map