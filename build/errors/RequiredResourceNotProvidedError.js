"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
class RequiredResourceNotProvidedError extends mink_1.BadRequestError {
    constructor(resourceType) {
        super(`<${resourceType}> should be provided via <x-resource-${resourceType}> header!`);
        this.name = 'RequiredResourceNotProvidedError';
    }
}
exports.RequiredResourceNotProvidedError = RequiredResourceNotProvidedError;
//# sourceMappingURL=RequiredResourceNotProvidedError.js.map