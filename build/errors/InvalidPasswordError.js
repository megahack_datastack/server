"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
class InvalidPasswordError extends mink_1.BadRequestError {
    constructor(identifier) {
        super('Login attempt with invalid credentials for account identified with \'' + identifier + '\' will be reported!');
        this.name = 'InavlidPasswordError';
    }
}
exports.InvalidPasswordError = InvalidPasswordError;
//# sourceMappingURL=InvalidPasswordError.js.map