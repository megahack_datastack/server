"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
class InvalidTokenError extends mink_1.BadRequestError {
    constructor(token) {
        super('Access attempt with invalid token\'' + token + '\' will be reported!');
        this.name = 'InvalidTokenError';
    }
}
exports.InvalidTokenError = InvalidTokenError;
//# sourceMappingURL=InvalidTokenError.js.map