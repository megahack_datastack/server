"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
class EmailIsNotUniqueError extends mink_1.BadRequestError {
    constructor(email) {
        super('There\'s already an account registered with \'' + email + '\' email address.');
        this.name = 'EmailIsNotUniqueError';
    }
}
exports.EmailIsNotUniqueError = EmailIsNotUniqueError;
//# sourceMappingURL=EmailIsNotUniqueError.js.map