"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
class UnauthorizedResourceAccessAttemptError extends mink_1.BadRequestError {
    constructor(resourceType, id) {
        super(`Detected unauthorized resource access attempt on <${resourceType}: ${id}>! This will be reported!`);
        this.name = 'UnauthorizedResourceAccessAttemptError';
    }
}
exports.UnauthorizedResourceAccessAttemptError = UnauthorizedResourceAccessAttemptError;
//# sourceMappingURL=UnauthorizedResourceAccessAttemptError.js.map