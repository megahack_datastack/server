"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
class UnresolvableResourceAccessError extends mink_1.BadRequestError {
    constructor(resourceType, id) {
        super(`Unresolvable resource access on <${resourceType}: ${id}>! This will be reported!`);
        this.name = 'UnresolvableResourceAccessError';
    }
}
exports.UnresolvableResourceAccessError = UnresolvableResourceAccessError;
//# sourceMappingURL=UnresolvableResourceAccessError.js.map