"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ferret_1 = require("@zuu/ferret");
const owl_1 = require("@zuu/owl");
const Document_1 = require("./Document");
const Contract_1 = require("./Contract");
const Signature_1 = require("./Signature");
let User = class User extends ferret_1.BaseEntity {
    constructor(email, password) {
        super();
        this.email = email;
        this.password = password;
    }
    has2FAActivated() {
        return !!this.twoFactorToken;
    }
    ;
    hasCompletedRegistration() {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield this.documents).length > 0;
        });
    }
};
__decorate([
    owl_1.Field(type => owl_1.ID),
    ferret_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], User.prototype, "id", void 0);
__decorate([
    owl_1.Field(),
    ferret_1.CreateDateColumn(),
    __metadata("design:type", Date)
], User.prototype, "created", void 0);
__decorate([
    owl_1.Field(),
    ferret_1.Column({
        unique: true
    }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    ferret_1.Column(),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    ferret_1.Column({ nullable: true }),
    __metadata("design:type", String)
], User.prototype, "faceId", void 0);
__decorate([
    owl_1.Field(type => [Document_1.Document], { nullable: true }),
    ferret_1.OneToMany(type => Document_1.Document, doc => doc.user, { lazy: true }),
    __metadata("design:type", Object)
], User.prototype, "documents", void 0);
__decorate([
    owl_1.Field(type => [Contract_1.Contract]),
    ferret_1.ManyToMany(type => Contract_1.Contract, c => c.signers, { lazy: true }),
    ferret_1.JoinTable(),
    __metadata("design:type", Object)
], User.prototype, "contracts", void 0);
__decorate([
    owl_1.Field(tyep => [Signature_1.Signature]),
    ferret_1.OneToMany(type => Signature_1.Signature, s => s.user, { lazy: true }),
    __metadata("design:type", Object)
], User.prototype, "signatures", void 0);
__decorate([
    ferret_1.Column({ nullable: true }),
    __metadata("design:type", String)
], User.prototype, "twoFactorToken", void 0);
__decorate([
    owl_1.Field(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Boolean)
], User.prototype, "has2FAActivated", null);
__decorate([
    owl_1.Field(type => Boolean),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], User.prototype, "hasCompletedRegistration", null);
User = __decorate([
    owl_1.ObjectType(),
    ferret_1.Entity(),
    __metadata("design:paramtypes", [String, String])
], User);
exports.User = User;
//# sourceMappingURL=User.js.map