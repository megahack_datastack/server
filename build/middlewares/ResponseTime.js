"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
const vet_1 = require("@zuu/vet");
let tag = vet_1.Debugger.tag('response-time-middleware');
let ResponseTime = class ResponseTime {
    constructor() {
        vet_1.Debugger.log(tag `Middleware loaded!`);
    }
    use(request, response, next) {
        request.time = { start: Date.now() };
        next();
    }
};
ResponseTime = __decorate([
    mink_1.Middleware({ type: 'before' }),
    __metadata("design:paramtypes", [])
], ResponseTime);
exports.ResponseTime = ResponseTime;
//# sourceMappingURL=ResponseTime.js.map