"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const vet_1 = require("@zuu/vet");
let Cypher = class Cypher {
    encode(data) {
        return new Buffer(data, 'ascii').toString('base64');
    }
    decode(data) {
        return new Buffer(data, 'base64').toString('ascii');
    }
    shuffle(input) {
        return input.split('').sort(_ => 0.5 - Math.random()).join('');
    }
};
Cypher = __decorate([
    vet_1.Singleton
], Cypher);
exports.Cypher = Cypher;
//# sourceMappingURL=Cypher.js.map