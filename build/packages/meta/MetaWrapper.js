"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MetaWrapper {
    wrapLogs(object, ...logs) {
        return Object.assign({}, object, { _: logs });
    }
}
exports.MetaWrapper = MetaWrapper;
//# sourceMappingURL=MetaWrapper.js.map