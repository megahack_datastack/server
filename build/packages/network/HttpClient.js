"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fetch = require("node-fetch");
class HttpClient {
    constructor(uri) {
        this.uri = uri;
    }
    ;
    post({ method, data, headers }) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = this.uri + method;
            headers = headers || [];
            data = data || {};
            headers['content-type'] = "application/json";
            let response = yield fetch(url, {
                body: JSON.stringify(data),
                cache: 'no-cache',
                credentials: 'same-origin',
                headers,
                method: 'POST',
                mode: 'cors',
                redirect: 'follow',
                referrer: 'no-referrer'
            });
            if (!response.ok) {
                throw yield response.json();
            }
            return yield response.json();
        });
    }
    ;
    get({ method, headers }) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = this.uri + method;
            headers = headers || [];
            headers['content-type'] = "application/json";
            let response = yield fetch(url, {
                cache: 'no-cache',
                credentials: 'same-origin',
                headers,
                method: 'GET',
                mode: 'cors',
                redirect: 'follow',
                referrer: 'no-referrer'
            });
            if (!response.ok) {
                throw yield response.json();
            }
            return yield response.json();
        });
    }
    ;
    /**
     * Classic REST request. The layer will decide if it will make a POST or a GET based on your options.
     * @param options Options and configuration of the request
     */
    call({ method, data, headers }) {
        return __awaiter(this, void 0, void 0, function* () {
            headers = headers || [];
            if (typeof data == "undefined" || data == null)
                return this.get({ method, headers });
            return this.post({ method, data, headers });
        });
    }
    ;
}
exports.HttpClient = HttpClient;
;
//# sourceMappingURL=HttpClient.js.map