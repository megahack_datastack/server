"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const UnresolvableResourceAccessError_1 = require("../../errors/UnresolvableResourceAccessError");
const UnauthorizedResourceAccessAttemptError_1 = require("../../errors/UnauthorizedResourceAccessAttemptError");
const Contract_1 = require("../../model/entities/Contract");
const Provider_1 = require("../../model/entities/Provider");
class HeadersContextFiller {
    static fill(user, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            let context = { user };
            let contractId = headers['x-resource-contract'];
            let providerId = headers['x-resource-provider'];
            if (!!contractId) {
                let contract = yield Contract_1.Contract.findOne(contractId);
                if (!contract)
                    throw new UnresolvableResourceAccessError_1.UnresolvableResourceAccessError('campagin', contractId);
                context.contract = contract;
            }
            if (!!providerId) {
                let provider = yield Provider_1.Provider.findOne(providerId);
                if (!provider)
                    throw new UnresolvableResourceAccessError_1.UnresolvableResourceAccessError('provider', providerId);
                if (provider.ownedBy != user.id)
                    throw new UnauthorizedResourceAccessAttemptError_1.UnauthorizedResourceAccessAttemptError('provider', providerId);
                context.provider = provider;
            }
            return context;
        });
    }
}
exports.HeadersContextFiller = HeadersContextFiller;
//# sourceMappingURL=HeadersContextFiller.js.map