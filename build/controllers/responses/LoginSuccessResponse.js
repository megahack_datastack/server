"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ConnectResponse_1 = require("./ConnectResponse");
class LoginSuccessResponse {
    constructor(user, token, refresh) {
        this.user = user;
        this.token = token.chars;
        this.refresh = refresh.chars;
        this.info = new ConnectResponse_1.ConnectResponse(user);
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            this.hasCompletedRegistration = yield this.user.hasCompletedRegistration();
            delete this.user;
        });
    }
}
exports.LoginSuccessResponse = LoginSuccessResponse;
//# sourceMappingURL=LoginSuccessResponse.js.map