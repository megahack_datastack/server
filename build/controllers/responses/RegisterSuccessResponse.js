"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConnectResponse_1 = require("./ConnectResponse");
class RegisterSuccessResponse {
    constructor(user, token, refresh) {
        this.token = token.chars;
        this.refresh = refresh.chars;
        this.info = new ConnectResponse_1.ConnectResponse(user);
    }
}
exports.RegisterSuccessResponse = RegisterSuccessResponse;
//# sourceMappingURL=RegisterSuccessResponse.js.map