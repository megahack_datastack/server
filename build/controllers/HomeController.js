"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
const vet_1 = require("@zuu/vet");
const MetaWrapper_1 = require("../packages/meta/MetaWrapper");
let tag = vet_1.Debugger.tag('home-controller');
let HomeController = class HomeController {
    constructor() {
        vet_1.Debugger.log(tag `New instance created!`);
    }
    index() {
        return {};
    }
};
__decorate([
    vet_1.Inject,
    __metadata("design:type", MetaWrapper_1.MetaWrapper)
], HomeController.prototype, "meta", void 0);
__decorate([
    mink_1.Get(),
    mink_1.Render('home'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], HomeController.prototype, "index", null);
HomeController = __decorate([
    mink_1.Controller('/'),
    __metadata("design:paramtypes", [])
], HomeController);
exports.HomeController = HomeController;
//# sourceMappingURL=HomeController.js.map