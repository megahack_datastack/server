"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
const vet_1 = require("@zuu/vet");
const AccountDoesNotExistError_1 = require("../errors/AccountDoesNotExistError");
const EmailIsNotUniqueError_1 = require("../errors/EmailIsNotUniqueError");
const InvalidPasswordError_1 = require("../errors/InvalidPasswordError");
const InvalidTokenError_1 = require("../errors/InvalidTokenError");
const User_1 = require("../model/entities/User");
const Cypher_1 = require("../packages/crypto/Cypher");
const Hash_1 = require("../packages/crypto/Hash");
const Salt_1 = require("../packages/crypto/Salt");
const Tokenize_1 = require("../packages/tokens/Tokenize");
const TokenScope_1 = require("../packages/tokens/TokenScope");
const LoginBody_1 = require("./bodies/LoginBody");
const RegisterBody_1 = require("./bodies/RegisterBody");
const ConnectResponse_1 = require("./responses/ConnectResponse");
const LoginSuccessResponse_1 = require("./responses/LoginSuccessResponse");
const RegisterSuccessResponse_1 = require("./responses/RegisterSuccessResponse");
let AuthController = class AuthController {
    constructor() {
        this.saltLength = this.securityConfig.hash.salt.length;
        this.saltSecret = this.securityConfig.hash.salt.secret;
    }
    register(register) {
        return __awaiter(this, void 0, void 0, function* () {
            let salt = this.salt.secret(this.cypher.shuffle(this.saltSecret), this.saltLength);
            let hashedPassword = this.hash.update(register.password, salt, Hash_1.HashingAlgorithm.SHA512);
            let compiledPassword = this.hash.compile(hashedPassword);
            let encodedPassword = this.cypher.encode(compiledPassword);
            let user = new User_1.User(register.email, encodedPassword);
            try {
                yield user.save();
            }
            catch (ex) {
                console.log(ex);
                throw new EmailIsNotUniqueError_1.EmailIsNotUniqueError(register.email);
            }
            let token = yield this.tokenize.create(user.id, TokenScope_1.TokenScope.AUTH);
            let refresh = yield this.tokenize.create(user.id, TokenScope_1.TokenScope.REFRESH, false);
            return new RegisterSuccessResponse_1.RegisterSuccessResponse(user, token, refresh);
        });
    }
    login(login) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield User_1.User.findOne({ email: login.email });
            if (!user)
                throw new AccountDoesNotExistError_1.AccountDoesNotExistError(login.email);
            let decodedPassword = this.cypher.decode(user.password);
            let decompiledPassword = this.hash.decompile(decodedPassword);
            if (!this.hash.same(login.password, decompiledPassword))
                throw new InvalidPasswordError_1.InvalidPasswordError(login.email);
            let token = yield this.tokenize.create(user.id, TokenScope_1.TokenScope.AUTH);
            let refresh = yield this.tokenize.create(user.id, TokenScope_1.TokenScope.REFRESH, false);
            let res = new LoginSuccessResponse_1.LoginSuccessResponse(user, token, refresh);
            yield res.init();
            return res;
        });
    }
    connect(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return new ConnectResponse_1.ConnectResponse(user);
        });
    }
    refresh(token) {
        return __awaiter(this, void 0, void 0, function* () {
            let refreshToken = yield this.tokenize.find(token, TokenScope_1.TokenScope.REFRESH);
            if (!refreshToken)
                throw new InvalidTokenError_1.InvalidTokenError(token);
            let user = yield User_1.User.findOne(refreshToken.target);
            let accessToken = yield this.tokenize.create(user.id, TokenScope_1.TokenScope.AUTH);
            return new LoginSuccessResponse_1.LoginSuccessResponse(user, accessToken, refreshToken);
        });
    }
};
__decorate([
    vet_1.Inject,
    __metadata("design:type", Salt_1.Salt)
], AuthController.prototype, "salt", void 0);
__decorate([
    vet_1.Inject,
    __metadata("design:type", Cypher_1.Cypher)
], AuthController.prototype, "cypher", void 0);
__decorate([
    vet_1.Inject,
    __metadata("design:type", Hash_1.Hash)
], AuthController.prototype, "hash", void 0);
__decorate([
    vet_1.Inject,
    __metadata("design:type", Tokenize_1.Tokenize)
], AuthController.prototype, "tokenize", void 0);
__decorate([
    vet_1.Configuration('security'),
    __metadata("design:type", Object)
], AuthController.prototype, "securityConfig", void 0);
__decorate([
    mink_1.Post('/register'),
    __param(0, mink_1.Body({ required: true, validate: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [RegisterBody_1.RegisterBody]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "register", null);
__decorate([
    mink_1.Post('/login'),
    __param(0, mink_1.Body({ required: true, validate: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [LoginBody_1.LoginBody]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "login", null);
__decorate([
    mink_1.Get('/'),
    __param(0, mink_1.CurrentUser({ required: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_1.User]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "connect", null);
__decorate([
    mink_1.Get('/refresh'),
    __param(0, mink_1.HeaderParam('x-refresh-token', { required: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "refresh", null);
AuthController = __decorate([
    mink_1.JsonController('/api/auth'),
    __metadata("design:paramtypes", [])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=AuthController.js.map