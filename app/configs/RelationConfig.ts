export interface RelationConfig {
    domain: string;
    frontend: string;
    self: string;
    sockets: string;
};