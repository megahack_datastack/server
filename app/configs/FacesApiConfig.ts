import { IConfig } from "@zuu/vet";

export interface FacesApiClient extends IConfig {
    subscriptionKey: string;
    regionEndpoint: string;
};