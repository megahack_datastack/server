import { Controller, Get, Render } from '@zuu/mink';
import { Debugger, Inject } from '@zuu/vet';
import { MetaWrapper } from '../packages/meta/MetaWrapper';

let tag = Debugger.tag('home-controller');

@Controller('/')
export class HomeController {
    @Inject meta: MetaWrapper;

    public constructor() {
        Debugger.log(tag`New instance created!`);
    }

    @Get()
    @Render('home')
    public index() {
        return { };
    }
}