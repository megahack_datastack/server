import { Post, BodyParam, CurrentUser, Get, Param, Res, Controller } from '@zuu/mink';
import { Uploaded } from '../model/entities/Uploaded';
import * as path from 'path';
import { UploadManager } from '../packages/upload/UploadManager';
import { UploadType } from '../packages/upload/Uploader';
import { UploadError } from '../errors/UploadError';
import { User } from '../model/entities/User';
import { DownloadError } from '../errors/DownloadError';
import { Response } from "express";
import * as fs from 'fs';

@Controller('/api/upload')
export class UploadController {
    private uploader: UploadManager = new UploadManager();

    @Post('')
    public async upload(
        @CurrentUser({ required: true }) user: User,
        @BodyParam('imageData') data: string,
        @BodyParam('extension', { required: false }) extension: string = "jpeg",
    ): Promise<any> {
        if(!(["jpeg", "png", "jpg", "pdf"].includes(extension))) throw new UploadError();

        let resource = new Uploaded();
        await resource.save();

        let root = path.join(__dirname, "../../storage/images");
        try {
            this.uploader.forRoot(root).save(resource.id + "." + extension, data, extension == "jpeg" ? UploadType.JPEG : extension == "jpg" ? UploadType.JPG : extension == "pdf" ? UploadType.PDF : UploadType.PNG);
        } catch(ex) {
            await resource.remove();
            return new UploadError();    
        }

        resource.path = path.join(root, resource.id + "." + extension);
        await resource.save();

        delete resource.path;
        return resource;
    }

    @Get('/:id')
    public async download(
        @Param('id') id: string,
        @Res() response: Response
    ) {
        let uploaded = await Uploaded.findOne(id);
        if(!uploaded) throw new DownloadError();
        
        return fs.createReadStream(uploaded.path);
    }
}