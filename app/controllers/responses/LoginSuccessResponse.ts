import { User } from '../../model/entities/User';
import { Token } from '../../model/entities/Token';
import { ConnectResponse } from './ConnectResponse';

export class LoginSuccessResponse {
    public token: string;
    public refresh: string;
    public info: ConnectResponse;
    public hasCompletedRegistration: Boolean;

    constructor(private user: User, token: Token, refresh: Token) {
        this.token = token.chars;
        this.refresh = refresh.chars;
        this.info = new ConnectResponse(user);
    }

    public async init() {
        this.hasCompletedRegistration = await this.user.hasCompletedRegistration();
        delete this.user;   
    }
}