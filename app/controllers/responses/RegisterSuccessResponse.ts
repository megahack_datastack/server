import { User } from '../../model/entities/User';
import { Token } from '../../model/entities/Token';
import { ConnectResponse } from './ConnectResponse';

export class RegisterSuccessResponse {
    public token: string;
    public refresh: string;
    public info: ConnectResponse;

    constructor(user: User, token: Token, refresh: Token) {
        this.token = token.chars;
        this.refresh = refresh.chars;

        this.info = new ConnectResponse(user);
    }
}