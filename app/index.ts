import { AuthedGraphQLController, Bootstrap, ZuuOptions } from '@zuu/bootstrap';
import { Action, UnauthorizedError } from '@zuu/mink';
import { Debugger, Runtime, ConfigManager } from '@zuu/vet';
import { ConnectionContext } from 'subscriptions-transport-ws';
import { AuthController } from './controllers/AuthController';
import { UploadController } from './controllers/UploadController';
import { HomeController } from './controllers/HomeController';
import { ResponseFormatter } from './interceptors/ResponseFormatter';
import { ListeningEventListener } from './listeners/ListeningEventListener';
import { SubscriptionServerListeningEventListener } from './listeners/SubscriptionServerListeningEventListener';
import { ResponseTime } from './middlewares/ResponseTime';
import { User } from './model/entities/User';
import { ExpressHandlebarsRenderer } from './modules/ExpressHandlebarsRenderer';
import { AuthContextChecker } from './packages/context/AuthContextChecker';
import { HeadersContextFiller } from './packages/context/HeadersContextFiller';
import { Timer } from './packages/timer/Timer';
import { MeResolver } from './resolvers/MeResolver';

import * as path from "path";
import { FaceApi } from './packages/faces/FaceApi';
import { Provider } from './model/entities/Provider';
import { Contract } from './model/entities/Contract';
import { ProviderResolver } from './resolvers/ProviderResolver';

Debugger.deafults();

let tag = Debugger.tag('app-index');

let options: ZuuOptions = {
    server: {
        port: parseInt(process.env['PORT']) || 4100,
        modules: [
            new ExpressHandlebarsRenderer({
                defaultLayout: 'main'
            })
        ]
    },
    currentUserChecker: async (action: Action): Promise<User> => {
        let token = action.request.headers['x-access-token'];
        return await AuthContextChecker.check(token);
    },
    graph: {
        contextFiller: async (user: User, headers: any): Promise<any> => {
            return await HeadersContextFiller.fill(user, headers);
        },
        subscriptionCurrentUserChecker: async (connectionParams: Object, webSocket: WebSocket, context: ConnectionContext): Promise<any> => {
            let token = connectionParams['x-access-token'];
            let user = await AuthContextChecker.check(token);
            if (!user) throw new UnauthorizedError();

            return { user, ...(await HeadersContextFiller.fill(user, options)) };
        }
    },
    listeners: [
        new ListeningEventListener,
        new SubscriptionServerListeningEventListener
    ],
    middlewares: [
        ResponseTime
    ],
    interceptors: [
        ResponseFormatter
    ],
    controllers: [
        HomeController,
        AuthController,
        UploadController,
        AuthedGraphQLController
    ],
    resolvers: [
        MeResolver,
        ProviderResolver
    ],
    cors: true
};

let lorem = `Lorem ipsum dolor sit amet, vivendo minimum facilisi nam ut, quis nihil facete no eum. Pri no minimum nominavi delectus, eam odio modus definitiones ut. Dolore comprehensam sit ea, vis pertinacia expetendis at. Est vocibus mentitum et.
Audire delenit mandamus pro in, ex sea veniam omittantur. Offendit consequat comprehensam nec at. Id elitr consequuntur nam. Ad per modus scripta conceptam, solet dolorum sea ad, has ipsum simul iisque ei.
Ei diceret denique ius, ne latine electram ius. Sed solet aliquando id, ea elitr dicant expetenda vel. Blandit platonem persequeris ea sea, ea eam rebum mazim. Admodum scribentur ei vix, vim te etiam dicunt. Vis debet ludus id. An duo nostrum perpetua.
No has dicant scripta. Patrioque referrentur ei vel, ius animal qualisque deterruisset in. Duo nibh iisque at, ius at omnium aperiam virtute. Ei vix alii dolorem minimum, sumo democritum usu ne, eam libris epicuri ex.
Tollit soluta dolores sit et, sea viris epicuri conclusionemque cu. Populo primis ut mea, duis delectus platonem quo cu. Vim ex falli volumus ullamcorper, altera melius persius sed id. Mel at debet labores, dicta decore menandri ei duo.`

async function testData() {
    let user = await User.findOne({ where: { email: "datastack@inno.ro" }});
    
    let provider = new Provider(user);
    provider.name = "InnoEnergy";
    await provider.save();

    // for(let i = 1; i <= 3; i++) {
        let contract = new Contract();
        contract.name = "Contract INNO";
        contract.description = `${lorem}`; 
        contract.preview = "fbea0baa-1d65-4634-8c92-4b3140189df8";
        contract.uploaded = "3aa354be-f640-47f8-a035-87842ecc1680";
        await contract.save();

        (await provider.contracts).push(contract);
        await provider.save();
        await contract.save();
    // }    
}

let timer = new Timer().reset();
Runtime.scoped(null, async _ => {
    Debugger.log(tag`Initialization began!`);
    let { app } = await Bootstrap.scope(options).run();
    return (typeof app != 'undefined' && app != null);
})
.then(async result => {
    Debugger.log(tag`Initialization succeeded! Took ${timer.stop().diff()}ms!`);
    // await testData();
})
.catch(Debugger.error);
