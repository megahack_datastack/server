import { BaseEntity, PrimaryGeneratedColumn, Entity, Column } from '@zuu/ferret';
import { Field, ID, ObjectType } from '@zuu/owl';

@ObjectType()
@Entity()
export class Authorization extends BaseEntity {
    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field({ nullable: false })
    @Column({ default: false })    
    public authorized: boolean;

    @Field({ nullable: false })
    @Column({ nullable: false })
    public user: string;

    public constructor(user: string, state: boolean) {
        super();

        this.user = user;
        this.authorized = state;
    }
}