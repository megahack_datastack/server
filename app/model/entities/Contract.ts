import { BaseEntity, PrimaryGeneratedColumn, Entity, Column, ManyToOne, OneToMany, ManyToMany, JoinTable } from '@zuu/ferret';
import { ObjectType, Field, ID } from '@zuu/owl';
import { Lazy } from '../../packages/async/Lazy';
import { User } from './User';
import { Provider } from './Provider';
import { Signature } from './Signature';
import { text } from 'body-parser';

@ObjectType()
@Entity()
export class Contract extends BaseEntity {
    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field({ nullable: false })
    @Column({ nullable: false })
    public uploaded: string;

    @Field({ nullable: false })
    @Column({ nullable: false })
    public preview: string;

    @Field({ nullable: false })
    @Column({ nullable: false })
    public name: string;

    @Field({ nullable: false })
    @Column({ nullable: false, type: 'text' })
    public description: string;

    @Field(type => Provider, { nullable: false })
    @ManyToOne(type => Provider, p => p.contracts, { lazy: true })
    public provider: Lazy<Provider>;

    @ManyToMany(type => User, u => u.contracts, { lazy: true })
    @JoinTable()
    public signers: Lazy<User[]>;

    @OneToMany(type => Signature, s => s.contract, { lazy: true })
    public signatures: Lazy<Signature[]>;
};