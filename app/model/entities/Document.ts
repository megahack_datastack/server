import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne } from '@zuu/ferret';
import { ObjectType, ID, Field, ArgsType } from '@zuu/owl';
import { User } from "./User";
import { Lazy } from '../../packages/async/Lazy';

export interface DocumentContract {
    documentType: string;
    documentNumber: string;
    surNames: string;
    givenNames: string;
    countryCode: string;
    issuingCountryCode: string;
    nationalityCountryCode: string;
    dayOfBirth: string;
    expirationDate: string;
    sex: string;
    personalNumber: string;
    checkDigitNumber: string;
    checkDigitPersonalNumber: string;
    checkDigitDayOfBirth: string;
    checkDigitExpirationDate: string;
    checkDigitFinal: string;
    allCheckDigitsValid: string;
    dayOfBirthObject: string;
    expirationDateObject: string;
    mrzString: string;
    uploaded: string;
};

@ArgsType()
export class DocumentArgs implements DocumentContract {
    @Field({ nullable: true })
    documentType: string;

    @Field({ nullable: true })
    documentNumber: string;

    @Field({ nullable: true })
    surNames: string;

    @Field({ nullable: true })
    givenNames: string;

    @Field({ nullable: true })
    countryCode: string;

    @Field({ nullable: true })
    issuingCountryCode: string;

    @Field({ nullable: true })
    nationalityCountryCode: string;

    @Field({ nullable: true })
    dayOfBirth: string;

    @Field({ nullable: true })
    expirationDate: string;

    @Field({ nullable: true })
    sex: string;

    @Field({ nullable: true })
    personalNumber: string;

    @Field({ nullable: true })
    checkDigitNumber: string;

    @Field({ nullable: true })
    checkDigitPersonalNumber: string;

    @Field({ nullable: true })
    checkDigitDayOfBirth: string;

    @Field({ nullable: true })
    checkDigitExpirationDate: string;

    @Field({ nullable: true })
    checkDigitFinal: string;

    @Field({ nullable: true })
    allCheckDigitsValid: string;

    @Field({ nullable: true })
    dayOfBirthObject: string;

    @Field({ nullable: true })
    expirationDateObject: string;

    @Field({ nullable: true })
    mrzString: string;

    @Field({ nullable: true })
    public uploaded: string;
};

@ObjectType()
@Entity()
export class Document extends BaseEntity {
    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    documentType: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    documentNumber: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    surNames: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    givenNames: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    countryCode: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    issuingCountryCode: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    nationalityCountryCode: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    dayOfBirth: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    expirationDate: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    sex: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    personalNumber: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    checkDigitNumber: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    checkDigitPersonalNumber: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    checkDigitDayOfBirth: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    checkDigitExpirationDate: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    checkDigitFinal: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    allCheckDigitsValid: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    dayOfBirthObject: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    expirationDateObject: string;

    @Field({ nullable: true })
    @Column({ default: '', nullable: true })
    mrzString: string;

    @Column({ nullable: true })
    public uploaded: string;

    @Column({ nullable: true })
    public faceId: string;

    @Field(type => User)
    @ManyToOne(type => User, user => user.documents, { lazy: true })
    public user: Lazy<User>;

    public constructor(input: DocumentContract) {
        super();

        if(!input) return;

        this.documentType = input.documentType;
        this.documentNumber = input.documentNumber;
        this.surNames = input.surNames;
        this.givenNames = input.givenNames;
        this.countryCode = input.countryCode;
        this.issuingCountryCode = input.issuingCountryCode;
        this.nationalityCountryCode = input.nationalityCountryCode;
        this.dayOfBirth = input.dayOfBirth;
        this.expirationDate = input.expirationDate;
        this.sex = input.sex;
        this.personalNumber = input.personalNumber;
        this.checkDigitNumber = input.checkDigitNumber;
        this.checkDigitPersonalNumber = input.checkDigitPersonalNumber;
        this.checkDigitDayOfBirth = input.checkDigitDayOfBirth;
        this.checkDigitExpirationDate = input.checkDigitExpirationDate;
        this.checkDigitFinal = input.checkDigitFinal;
        this.allCheckDigitsValid = input.allCheckDigitsValid;
        this.dayOfBirthObject = input.dayOfBirth;
        this.expirationDateObject = input.expirationDateObject;
        this.mrzString = input.mrzString;
        this.uploaded = input.uploaded;
    };
};