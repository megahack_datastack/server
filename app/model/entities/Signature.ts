import { BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, Entity } from '@zuu/ferret';
import { Lazy } from '../../packages/async/Lazy';
import { Contract } from './Contract';
import { Field, ID, ObjectType } from '@zuu/owl';
import { User } from './User';

@ObjectType()
@Entity()
export class Signature extends BaseEntity {

    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field(type => Contract)
    @ManyToOne(type => Contract, c => c.signatures, { lazy: true })
    public contract: Lazy<Contract>;

    @Field(type => User)
    @ManyToOne(type => User, u => u.signatures, { lazy: true })
    public user: Lazy<User>;

    @Field({ nullable: false })
    @Column({ nullable: false })
    public uploaded: string;
};