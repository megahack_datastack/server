import { ObjectType, Field } from '@zuu/owl';
import { Contract } from './Contract';
import { Signature } from './Signature';

@ObjectType()
export class SignedContract {

    @Field(type => Contract, { nullable: false })
    public contract: Contract;

    @Field(type => Signature, { nullable: true })
    public signature: Signature;

    @Field({ nullable: true })
    public qr: string;
}