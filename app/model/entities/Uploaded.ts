import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, CreateDateColumn } from '@zuu/ferret';

@Entity()
export class Uploaded extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @CreateDateColumn()
    public created: Date;

    @Column({ nullable: true })
    public path: string;
};