import { BaseEntity, Column, CreateDateColumn, Entity, Index, PrimaryGeneratedColumn, OneToMany, OneToOne, ManyToMany, JoinTable } from '@zuu/ferret';
import { Field, ID, ObjectType } from '@zuu/owl';
import { Lazy } from '../../packages/async/Lazy';
import { Document } from "./Document";
import { Contract } from './Contract';
import { Signature } from './Signature';

@ObjectType()
@Entity()
export class User extends BaseEntity {
    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field()
    @CreateDateColumn()
    public created: Date;

    @Field()
    @Column({
        unique: true
    })
    public email: string;

    @Column()
    public password: string;

    @Column({ nullable: true })
    public faceId: string;

    @Field(type => [Document], {nullable: true})
    @OneToMany(type => Document, doc => doc.user, { lazy: true })
    public documents: Lazy<Document[]>;

    @Field(type => [Contract])
    @ManyToMany(type => Contract, c => c.signers, { lazy: true })
    @JoinTable()
    public contracts: Lazy<Contract[]>;

    @Field(tyep => [Signature])
    @OneToMany(type => Signature, s => s.user, { lazy: true })
    public signatures: Lazy<Signature[]>;

    @Column({ nullable: true })
    public twoFactorToken: string;

    @Field()
    public has2FAActivated(): Boolean {
        return !!this.twoFactorToken;
    };

    @Field(type => Boolean)
    public async hasCompletedRegistration(): Promise<Boolean> {
        return (await this.documents).length > 0;
    }

    public constructor(email: string, password: string) {
        super();
        
        this.email = email;
        this.password = password;
    }
}