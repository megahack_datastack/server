import { BaseEntity, PrimaryGeneratedColumn, Entity, OneToMany, Column } from '@zuu/ferret';
import { ObjectType, Field, ID } from '@zuu/owl';
import { Lazy } from '../../packages/async/Lazy';
import { Contract } from './Contract';
import { User } from './User';

@ObjectType()
@Entity()
export class Provider extends BaseEntity {
    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field()
    @Column({ nullable: false })
    public name: string;

    @Field(type => [Contract])
    @OneToMany(type => Contract, c => c.provider, { lazy: true })
    public contracts: Lazy<Contract[]>;

    @Column({ nullable: false })
    public ownedBy: string;

    constructor(user: User) {
        super();
        
        if(!user) return;
        this.ownedBy = user.id;
    }
};