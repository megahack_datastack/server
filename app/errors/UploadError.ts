import { BadRequestError } from '@zuu/mink';

export class UploadError extends BadRequestError {
    constructor() {
        super('There was a problem while uploading your file!');
        this.name = 'UploadError';
    }
}