import { BadRequestError } from '@zuu/mink';

export class DownloadError extends BadRequestError {
    constructor() {
        super('There was a problem while downloading your file!');
        this.name = 'DownloadError';
    }
}