import { Resolver, Query, Ctx, Mutation, Args, Arg } from '@zuu/owl';
import { User } from '../model/entities/User';
import { DocumentArgs, Document } from '../model/entities/Document';
import { Uploaded } from '../model/entities/Uploaded';
import { FaceApi } from '../packages/faces/FaceApi';
import { Authorization } from '../model/entities/Authorization';
import { Signature } from '../model/entities/Signature';
import { Contract } from '../model/entities/Contract';
import { RequiredResourceNotProvidedError } from '../errors/RequiredResourceNotProvidedError';
import { SignedContract } from '../model/entities/SignedContract';
import * as speakeasy from "speakeasy";
import * as qr from "qrcode";
import * as fs from "fs";
import * as path from 'path';

@Resolver()
export class MeResolver {
    @Query(returns => User)
    public async me(
        @Ctx('user') user: User
    ): Promise<User> {
        return user;
    };

    @Mutation(returns => Document)
    public async document(
        @Args(type => DocumentArgs) args: DocumentArgs,
        @Ctx('user') user: User
    ): Promise<Document> {
        let doc = new Document(args);
        await doc.save();
        
        (await user.documents).push(doc);
        if(doc.uploaded) {
            let uploaded = await Uploaded.findOne(doc.uploaded);
            if(!!uploaded) {
                let faces = new FaceApi();
                let faceResult = await faces.uploadFaces(uploaded.path);
                if(faceResult.length != 1) {
                    throw new Error('Invalid photo!');
                }
                let face = faceResult[0];
                doc.faceId = face.faceId;
                user.faceId = face.faceId;
            }
        }

        await user.save();
        await doc.save();

        return doc;
    };

    @Mutation(type => Authorization)
    public async authorize(
        @Arg('uploaded', { nullable: false }) uploadedId: string,
        @Ctx('user') user: User
    ): Promise<Authorization> {
        let auth;
        if(!user.faceId) {
            auth = new Authorization(user.id, false);
            await auth.save();
            return auth;
        }
        
        let uploaded = await Uploaded.findOne(uploadedId);
        if(!uploaded) {
            auth = new Authorization(user.id, false);
            await auth.save();
            return auth;
        } 

        try {
            let faces = new FaceApi();
            let faceResult = await faces.uploadFaces(uploaded.path);    
            if(faceResult.length != 1) {
                auth = new Authorization(user.id, false);
                await auth.save();
                return auth;
            }
            let face = faceResult[0];
            let match = await faces.compareFaces(user.faceId, face.faceId);
            console.log(face, match);
            if(!match.isIdentical) {
                auth = new Authorization(user.id, false);
                await auth.save();
                return auth;
            }
        } catch(ex) {
            auth = new Authorization(user.id, false);
            await auth.save();           
            return auth;
        }

        auth = new Authorization(user.id, true);
        await auth.save();
        return auth;
    };

    @Mutation(type => Authorization)
    public async authorizeUsing2FA(
        @Arg('tfa', { nullable: false }) tfa: string,
        @Ctx('user') user: User
    ): Promise<Authorization> {
        if(!user.has2FAActivated) {
            let auth = new Authorization(user.id, false);
            await auth.save();
            return auth;
        }

        let verified = speakeasy.totp.verify({
            secret: user.twoFactorToken,
            encoding: 'base32',
            token: tfa
        }) || false;

        let auth = new Authorization(user.id, verified);
        await auth.save();
        return auth;
    };

    @Mutation(type => Boolean)
    public async activate2FA(
        @Arg('authorization', { nullable: false }) authorizationId: string,
        @Ctx('user') user: User
    ): Promise<Boolean> {
        let authorization = await Authorization.findOne(authorizationId);
        if(!authorization) return false;

        if(!authorization.authorized) return false;
        if(authorization.user != user.id) return false;
 
        let token = speakeasy.generateSecret({length: 20});
        user.twoFactorToken = token.base32;
        await user.save();

        let code = await qr.toDataURL(token.otpauth_url);
        fs.writeFileSync(path.join(__dirname, "../../qr.html"), `
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>QR</title>
            </head>
            <body>
                <img src="${code}">
            </body>
            </html>
        `)

        return true;
    };

    @Mutation(type => Signature)
    public async sign(
        @Arg('authorization', { nullable: false }) authorizationId: string,
        @Arg('uploaded', { nullable: false }) uploadedId: string,
        @Arg('contract', { nullable: false }) contractId: string,
        @Ctx('user') user: User
    ): Promise<Signature> {
        let uploaded = await Uploaded.findOne(uploadedId);
        if(!uploaded) return null;

        let authorization = await Authorization.findOne(authorizationId);
        if(!authorization) return null;

        if(!authorization.authorized) return null;
        if(authorization.user != user.id) return null;

        let contract = await Contract.findOne(contractId);
        if(!contract) return null;

        let signature = new Signature();
        signature.uploaded = uploadedId;
        await user.save();

        (await contract.signatures).push(signature);

        await signature.save();
        await contract.save();
        await user.save();

        (await user.signatures).push(signature);        
        (await user.contracts).push(contract);

        await signature.save();
        await contract.save();
        await user.save();

        return signature;
    }

    @Query(type => SignedContract, { nullable: true })
    public async signedContract(
        @Ctx('contract') contract: Contract,
        @Ctx('user') user: User    
    ): Promise<SignedContract> {
        if(!contract) throw new RequiredResourceNotProvidedError('contract');
        
        let sc = new SignedContract();
        sc.contract = contract;

        let signature = await Signature.findOne({ where: { user, contract }});
        if(!!signature) {
            sc.signature = signature;
            sc.qr = await qr.toDataURL(signature.id);
        }
        return sc;
    }
}