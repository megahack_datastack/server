import { Resolver, Arg, Query, Ctx } from '@zuu/owl';
import { Provider } from '../model/entities/Provider';
import { Signature } from '../model/entities/Signature';
import { Contract } from '../model/entities/Contract';

import * as qr from "qrcode";
import * as path from 'path';
import * as fs from 'fs';

@Resolver()
export class ProviderResolver {
    @Query(returns => Signature, { nullable: true })
    public async signature(
        @Arg('sign') signId: string,
        @Ctx('provider') provider: Provider
    ): Promise<Signature> {
        if(!provider) return null;

        let signature = await Signature.findOne(signId);
        if(!signature) return null;

        if((await (await signature.contract).provider).id != provider.id) return null;

        return signature;
    };

    @Query(returns => [Signature], { nullable: false })
    public async signaturesOfContract(
        @Ctx('contract') contract: Contract,
        @Ctx('provider') provider: Provider
    ): Promise<Signature[]> {
        if(!provider) return [];
        if(provider.id != (await contract.provider).id) return [];

        return await Signature.find({ where: { provider } }) || [];
    };

    @Query(returns => String, { nullable: false })
    public async qrOfContract(
        @Ctx('contract') contract: Contract
    ): Promise<string> {
        if(!contract) return "";
        let code = await qr.toDataURL(contract.id);
        fs.writeFileSync(path.join(__dirname, "../../qr.html"), `
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>QR</title>
            </head>
            <body>
                <img src="${code}">
            </body>
            </html>
        `)
        return code;
    }

    @Query(returns => Provider, { nullable: true })
    public async provider(
        @Ctx('provider') provider: Provider 
    ): Promise<Provider> {
        return provider;
    }
}