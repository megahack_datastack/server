import * as path from 'path';
import * as fs from 'fs';

export class UploadType {
    public static PNG = /^data:image\/png;base64,/
    public static JPEG = /^data:image\/jpeg;base64,/
    // public static JPG = /^data:image\/jpg;base64,/
    public static JPG = /^data:image\/\*;charset=utf-8;base64,/
    public static PDF = /^data:application\/pdf;base64,/
};

export class Uploader {

    constructor(private root: string) { }
    
    public save(name: string, data: string, type: RegExp = UploadType.PNG) {
        data = data.replace(type, "");
        fs.writeFileSync(path.join(this.root, name), data, "base64");
    };
};