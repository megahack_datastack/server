import { Uploader } from "./Uploader";
import * as path from 'path';

export class UploadManager {
    public forRoot(path: string): Uploader {
        return new Uploader(path);
    }  
};