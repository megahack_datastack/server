export interface MetaLog {
    tag: string;
    value: string | number | boolean;
    unit?: string;  
};