import { MetaLog } from './MetaLog';
export class MetaWrapper {
    public wrapLogs(object: any, ...logs: MetaLog[]) {
        return { ...object, _: logs };
    }
}