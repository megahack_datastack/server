import { Configuration } from "@zuu/vet";
import FaceClient from "azure-cognitiveservices-face";
import * as fs from "fs";
import { CognitiveServicesCredentials } from "ms-rest-azure";
import { FacesApiClient } from "../../configs/FacesApiConfig";
import { DetectedFace, VerifyResult } from "azure-cognitiveservices-face/lib/models";

const FaceAPIClient = require("azure-cognitiveservices-face");

export class FaceApi {
  @Configuration("faces.api")
  private faceApiConfig: FacesApiClient;

  private _client: FaceClient;
  private _options = {
    returnFaceId: true,
    returnFaceAttributes: [
      "age",
      "gender",
      "headPose",
      "smile",
      "facialHair",
      "glasses",
      "emotion",
      "hair",
      "makeup",
      "occlusion",
      "accessories",
      "exposure",
      "noise"
    ]
  };

  constructor() {
    let credentials = new CognitiveServicesCredentials(
      this.faceApiConfig.subscriptionKey
    );
    this._client = new FaceAPIClient(
      credentials,
      this.faceApiConfig.regionEndpoint
    );
  };

  public async uploadFaces(path: fs.PathLike): Promise<DetectedFace[]> {
    let stream = fs.createReadStream(path);
    let data = await this._client.face.detectWithStreamWithHttpOperationResponse(stream, this._options);
    return data.body;
  };

  public async compareFaces(face1: string, face2: string): Promise<VerifyResult> {
    let data = await this._client.face.verifyFaceToFaceWithHttpOperationResponse(face1, face2);
    return data.body;
  };
}
