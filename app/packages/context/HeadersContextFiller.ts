import { UnresolvableResourceAccessError } from '../../errors/UnresolvableResourceAccessError';
import { UnauthorizedResourceAccessAttemptError } from '../../errors/UnauthorizedResourceAccessAttemptError';
import { User } from '../../model/entities/User';
import { Context } from './Context';
import { Contract } from '../../model/entities/Contract';
import { Provider } from '../../model/entities/Provider';

export class HeadersContextFiller {
    public static async fill(user: User, headers: any): Promise<Object> {
        let context: Context = { user };
 
        let contractId = headers['x-resource-contract'];
        let providerId = headers['x-resource-provider'];

        if(!!contractId) {
            let contract = await Contract.findOne(contractId);
            if(!contract) throw new UnresolvableResourceAccessError('campagin', contractId);

            context.contract = contract;            
        }

        if(!!providerId) {
            let provider = await Provider.findOne(providerId);
            if(!provider) throw new UnresolvableResourceAccessError('provider', providerId);
            if(provider.ownedBy != user.id) throw new UnauthorizedResourceAccessAttemptError('provider', providerId);
            
            context.provider = provider;
        }

        return context;
    }
}