import { User } from '../../model/entities/User';
import { Contract } from '../../model/entities/Contract';
import { Provider } from '../../model/entities/Provider';

export interface Context { 
    user?: User;
    contract?: Contract;
    provider?: Provider;
    serverToken?: string;
};