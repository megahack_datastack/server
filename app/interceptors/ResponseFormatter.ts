import { Interceptor, InterceptorInterface, Action } from '@zuu/mink';
import { Debugger } from '@zuu/vet';
import { ReadStream } from 'fs';

let tag = Debugger.tag('response-formatter-interceptor');

@Interceptor()
export class ResponseFormatter implements InterceptorInterface {
    constructor() {
        Debugger.log(tag`Interceptor loaded!`);
    }
    
    intercept(action: Action, result: any) {
        if(result instanceof ReadStream) return result;

        action.request.time.end = Date.now();
        action.request.time.spent = Date.now() - action.request.time.start;
        
        let logs = result._;
        delete result._;

        if(!result.data) {
            result = { data: result };
        }

        result.meta = {
            headers: action.request.headers,
            body: action.request.body,
            query: action.request.query,
            params: action.request.params,
            time: { 
                now: Date.now(),
                start: action.request.time.start,
                spent: action.request.time.spent
            },
            logs
        };
        delete result.meta.body.imageData;
        
        return result;
    }
}